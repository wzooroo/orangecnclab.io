function lpt2gpio()
{
    var src = document.querySelector("#source");
    var out = document.querySelector("#output");
    var notes_ru = document.querySelector("#conv_notes_ru");
    var notes_en = document.querySelector("#conv_notes_en");
    var in_all = src.innerText.match(/parport\.[0-9]+\.pin\-[0-9]+\-in/g);
    var out_all = src.innerText.match(/parport\.[0-9]+\.pin\-[0-9]+\-out/g);
    
    if ( out_all == null || typeof(out_all) != "object" || typeof(out_all.length) != "number" ) out_all = [];
    if ( in_all == null || typeof(in_all) != "object" || typeof(in_all.length) != "number" ) in_all = [];
    
    var in_uniq = [], out_uniq = [];

    for ( var i = out_all.length; i--; )
    {
        if ( !out_uniq.includes(out_all[i]) ) out_uniq.push(out_all[i]);
    }
    for ( var i = in_all.length; i--; )
    {
        if ( !in_uniq.includes(in_all[i]) ) in_uniq.push(in_all[i]);
    }

    var total_cnt = in_uniq.length + out_uniq.length;
    var pins = [
        // main GPIO socket
        3,5,7,8,10,11,12,13,15,16,18,19,21,22,23,24,26,27,28,29,31,32,33,35,36,37,38,40,41,42,
        // camera (CSI) GPIO socket
        'PE13','PE12','PE15','PE3','PE2','PE11','PE1','PE10','PE9','PE0','PE8','PE4','PE7','PE5','PE6' ];
    
    if ( total_cnt > pins.length ) 
    {
        notes_ru.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> "
            + "Не хватает " + (total_cnt - pins.length) + " GPIO пинов для конвертации. "
            + "В вашем файле в общем используется "+total_cnt+" LPT пинов. "
            + "А на плате доступно всего "+pins.length+" GPIO пинов</p>";
        notes_en.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> "
            + "Not enough GPIO pins for conversation, you need " + (total_cnt - pins.length) + " pins more. "
            + "Your HAL code totally uses "+total_cnt+" LPT pins. "
            + "But a mini PC has only "+pins.length+" pins</p>";
    }
    else if ( !total_cnt )
    {
        notes_ru.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> "
            + "В вашем HAL коде не используются LPT пины</p>";
        notes_en.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> "
            + "There are no LPT pins inside this HAL code</p>";
    }
    else 
    {
        var p = 0, s = src.innerText, reg = 0;

        // replace pins
        for ( var i = out_uniq.length; i--; p++ )
        {
            reg = new RegExp(out_uniq[i].replace(/([\.\-])/g, '\$1'), "g");
            s = s.replace(reg, 'opi_gpio.pin-' + (pins[p].toString().length < 2 ? '0'+pins[p] : pins[p]) + '-out');
        }
        for ( var i = in_uniq.length; i--; p++ )
        {
            reg = new RegExp(in_uniq[i].replace(/([\.\-])/g, '\$1'), "g");
            s = s.replace(reg, 'opi_gpio.pin-' + (pins[p].toString().length < 2 ? '0'+pins[p] : pins[p]) + '-in');
        }

        // replace loadrt string
        s = s.replace
        (
            /loadrt[\ \t]+hal_parport[^\r\n]*([\r\n]|$)*/g,
            'loadrt opi_gpio' 
                + (out_uniq.length ? ' output_pins='+pins.slice(0, out_uniq.length).join(',') : '')
                + (in_uniq.length ? ' input_pins='+pins.slice(out_uniq.length, total_cnt).join(',') : '')
                + "$1"
        );

        // replace others
        s = s.replace(/parport\.[0-9]+\./g, 'opi_gpio.');

        out.innerHTML = s;
        src.scrollTo(0,0);
        out.scrollTo(0,0);
        notes_ru.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> Готово.</p>";
        notes_en.innerHTML = "<p><strong>hal_parport &middot;> opi_gpio:</strong> Done.</p>";

        if ( hl_code != undefined && typeof(hl_code) == "function" ) hl_code();
    }
}

var btn = document.querySelector("#conv_btn");
btn.addEventListener("click", lpt2gpio);
