_LNG = function ()
{
    // languages list data
    this.code       = new Array;
    this.current    = 0;

    // find page languages
    var e_list = document.querySelectorAll("*[lang]");
    for ( var e = 0, size = e_list.length, codes = ""; e < size; e++ )
    {
        if ( ! codes.match(e_list[e].lang) )
        {
            codes += e_list[e].lang + ",";
            this.code[ this.code.length ] = e_list[e].lang;
        }
    }
}

_LNG.prototype.createSelector = function()
{
    if ( this.code.length <= 0 ) return;

    var select_box = document.querySelector(".lang_selector");
    if ( !select_box ) return;
    
    var select = document.createElement("SELECT");

    for ( var c = 0, size = this.code.length, option; c < size; c++ )
    {
        option = document.createElement("OPTION");
        option.value        = c;
        option.innerHTML    = this.code[c].toUpperCase();
        select.appendChild(option);
    }

    select.onchange = function() // on user language selection
    {
        document.cookie =
            "language=" + _lng.code[this.value] + "; " + 
            "path=/; " + 
            "max-age=" + (60*60*24*365) + "; ";

        _lng.current = this.value;
        _lng.update();
    };
    select_box.appendChild(select);
}

_LNG.prototype.setLangFromBrowser = function()
{
    if ( this.code.length <= 0 ) return;

    var select = document.querySelector(".lang_selector > select");
    var lang = navigator.language || navigator.userLanguage;
    var cookie = document.cookie.match(/(^|[^\w])language=(\w+)/);

    if ( cookie ) lang = cookie[2];

    for ( var c = 0, size = this.code.length; c < size; c++ )
    {
        if ( lang.match(this.code[c]) )
        {
            if ( select ) select.selectedIndex = c;
            this.current = c;
            break;
        }
    }
}

_LNG.prototype.update = function()
{
    if ( this.code.length <= 0 ) return;

    var e_list = document.querySelectorAll("*[lang]");
    for ( var e = 0, size = e_list.length; e < size; e++ )
    {
        if ( e_list[e].nodeName == "TITLE" ) continue;
        if ( e_list[e].lang == this.code[this.current] )
        {
            e_list[e].classList.remove("lang_hide");
        }
        else
        {
            e_list[e].classList.add("lang_hide");
        }
    }
    
    // update title
    var el = document.querySelectorAll("title[lang]");
    if ( el && el.length > 1 )
    {
        var v = el[0].innerHTML;
        for ( var e = el.length; e--; )
        {
            // store original data
            if ( !el[e].title || el[e].title == "undefined" ) el[e].title = el[e].innerHTML;
            if ( el[e].lang == this.code[this.current] ) { v = el[e].title; break; }
        }
        el[0].innerHTML = v;
    }
}

_LNG.prototype.getCurrentLangCode = function()
{
    if ( this.code.length > 0 ) return this.code[0];
    return "";
}




_lng = new _LNG;
_lng.createSelector();
_lng.setLangFromBrowser();
_lng.update();
